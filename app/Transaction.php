<?php

namespace dollar;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{

    protected $table = 'transactions';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id',
        'type',
        'account',
        'amount',
        'observation',
        'vendor',
        'amount_extern',
        'exchange',
        'created_at',
        'updated_at',

    ];

}
