<?php

namespace dollar;

use Illuminate\Database\Eloquent\Model;

class BankAccount extends Model
{

    protected $table = 'bank_account';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        'id',
        'dni',
        'bank',
        'number',
        'balance',
        'type',
        'owner',
    ];

}
