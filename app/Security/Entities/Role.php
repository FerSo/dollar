<?php

namespace dollar\Security\Entities;

use Illuminate\Database\Eloquent\Model;
use dollar\User;


class Role extends Model
{
    /**
     * Table
     *
     * @var string
     */
    protected $table = 'roles';



    /**
     * Primary key
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Timestamps
     *
     * @var bool
     */
    public $timestamps = false;

    public $fillable = [
        'id',
        'name',
    ];
}
