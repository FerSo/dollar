<?php

namespace dollar\Http\Middleware;


use Closure;
use Illuminate\Support\Facades\Gate;

class Permission
{

    public function handle($request, Closure $next, $permission)
    {
        $unique_permissions = \Session::get('permissions');
        $unique_roles = \Session::get('roles');
        $array_permission = [];
        $role = '';
        $route = '';
        foreach($unique_roles as $rol){
            $role = $rol;
        }
        foreach($unique_permissions as $permi){
            $array_permission[$permi] = $permi;
        }
        if (\Auth::check()) {
            if (!isset($array_permission[$permission])) {
                switch ($role) {
                    case 1:
                        $route = 'admin.index';
                        break;
                    case 2:
                        $route = 'buyer.index';
                        break;
                    case 4:
                        $route = 'seller.index';
                        break;
                    case 3:
                        $route = 'reseller.index';
                        break;
                    case 5:
                        $route = 'auditor.index';
                        break;
                }

                return redirect()->route($route);
            }
        }
        return $next($request);
    }
}