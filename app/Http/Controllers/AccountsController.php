<?php

namespace dollar\Http\Controllers;

use dollar\Bank;
use dollar\BankAccount;
use dollar\Security\Enums\Roles;
use dollar\TypeAccount;
use Illuminate\Http\Request;

class AccountsController extends Controller
{

    public function __construct()
    {

        $this->middleware('auth');
    }

    public function create()
    {
        $role = session()->get('roles');

        if ($role[0] === Roles::$buyer) {
            $banks = Bank::where('country', 'US')->get();
        } else {
            $banks = Bank::where('country', 'VE')->get();
        }

        $types = TypeAccount::all();

        return view('pages.back.accounts.add')->with(['banks' => $banks, 'types' => $types]);
    }

    public function store(Request $request)
    {
        try {
            $this->validate($request, [
                'bank' => 'required',
                'email' => 'required',
                'dni' => 'required',
                'beneficiary' => 'required',
                'number' => 'required',
                'type' => 'required'
            ]);

            $account = new BankAccount();
            $account->fill($request->all());
            $account->user = \Auth::user()->id;
            $account->save();

            $response = [
                'status' => 'OK',
                'message' => 'Cuenta Guardada'
            ];

            return redirect()->route('accounts.all')->with($response);

        } catch (\Exception $exception) {
            $response = [
                'status' => 'FAILED',
                'message' => 'Error al guardar Cuenta'
            ];
            return redirect()->back()->with($response);
        }
    }

    public function edit($id)
    {
        try {
            $role = session()->get('roles');

            if ($role[0] === Roles::$buyer) {
                $banks = Bank::where('country', 'US')->get();
            } else {
                $banks = Bank::where('country', 'VE')->get();
            }

            $types = TypeAccount::all();
            $account = BankAccount::find($id);

            return view('pages.back.accounts.edit')->with(['account' => $account, 'types' => $types, 'banks' => $banks]);

        } catch (\Exception $exception) {
            return redirect()->back();
        }
    }

    public function delete($id)
    {
        try {
            $account = BankAccount::find($id);

           $account->delete();
            $response = [
                'status' => 'OK',
                'message' => 'Cuenta Eliminada'
            ];
            return redirect()->route('accounts.all')->with($response);
        } catch (\Exception $exception) {
            return redirect()->back();
        }
    }

    public function update(Request $request, $id)
    {

        try {
            $this->validate($request, [
                'bank' => 'required',
                'email' => 'required',
                'dni' => 'required',
                'beneficiary' => 'required',
                'number' => 'required',
                'type' => 'required'
            ]);

            $account = BankAccount::find($id);
            $account->fill($request->all());
            $account->save();

            $response = [
                'status' => 'OK',
                'message' => 'Cuenta actualizado'
            ];

            return redirect()->route('accounts.all')->with($response);

        } catch (\Exception $exception) {
            $response = [
                'status' => 'FAILED',
                'message' => 'Error al Actualizar Cuenta'
            ];
            return redirect()->back()->with($response);
        }
    }

    public function all()
    {
        try {
            $accounts = BankAccount::where('user', \Auth::user()->id)->get();

            foreach ($accounts as $item) {
                $bank = Bank::find($item->bank);
                $item->bank_name = $bank->name;
            }

            return view('pages.back.accounts.all')->with('accounts', $accounts);

        } catch (\Exception $exception) {
            return redirect()->back();
        }
    }

    public function check_accounts(){
        $accounts = count(BankAccount::where('user', \Auth::user()->id)->get());
        return $accounts;

    }

    public function accounts_seller(Request $request)
    {
        try {
            $seller = $request->get('offerer');
            $accounts = BankAccount::where('user', $seller)->get();

            foreach ($accounts as $item) {
                $bank = Bank::find($item->bank);
                $item->bank_name = $bank->name;
            }

            return $accounts;

        } catch (\Exception $exception) {
            return 'fail';
        }
    }
}
