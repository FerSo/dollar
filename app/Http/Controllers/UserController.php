<?php

namespace dollar\Http\Controllers;

use dollar\Security\Enums\Roles;
use dollar\Security\Repositories\RoleRepo;
use Illuminate\Http\Request;

use dollar\User;


class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function create()
    {
        return view('pages.back.users.add');
    }

    public function new_client()
    {
        return view('pages.back.users.add_client');
    }


    public function new_reseller()
    {
        $sellers = User::where('type', 4)->get();
        return view('pages.back.users.add_reseller')->with('sellers', $sellers);
    }

    public function store(Request $request)
    {


        try {
            $roleRepo = new RoleRepo();
            $this->validate($request, [
                'username' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required',
                'phone' => 'required',
                'address' => 'required',
                'password' => 'required|confirmed',
            ]);

            $user = new User();
            $user->fill([
                'username' => $request->get('username'),
                'first_name' => $request->get('first_name'),
                'last_name' => $request->get('last_name'),
                'email' => $request->get('email'),
                'referer' => \Auth::user()->id,
                'phone' => $request->get('phone'),
                'address' => $request->get('address'),
                'password' => $request->get('password'),
                'type' => 4
            ]);
            $user->save();
            $roleRepo->addRole($user->id, Roles::$seller);
            $response = [
                'status' => 'OK',
                'message' => 'Usuario Guardado'
            ];

            return redirect()->route('users.all')->with($response);

        } catch (\Exception $exception) {
            $response = [
                'status' => 'FAILED',
                'message' => 'Error al guardar Usuario'
            ];
            return redirect()->back()->with($response);
        }
    }

    public function store_client(Request $request)
    {
        try {
            $roleRepo = new RoleRepo();
            $this->validate($request, [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required',
                'phone' => 'required',
                'address' => 'required',
                'type' => 'required'
            ]);
            $user = new User();
            $user->fill([
                'username' => $request->get('first_name'),
                'first_name' => $request->get('first_name'),
                'last_name' => $request->get('last_name'),
                'email' => $request->get('email'),
                'referer' => \Auth::user()->id,
                'phone' => $request->get('phone'),
                'address' => $request->get('address'),
                'password' => '123456',
                'type' => 3
            ]);
            $user->save();
            $roleRepo->addRole($user->id, Roles::$seller);
            $response = [
                'status' => 'OK',
                'message' => 'Cliente Guardado'
            ];

            return redirect()->route('users.all.client', [$user->id])->with($response);

        } catch (\Exception $exception) {
            $response = [
                'status' => 'FAILED',
                'message' => 'Error al guardar Usuario'
            ];
            return redirect()->back()->with($response);
        }
    }

    public function store_reseller(Request $request)
    {
        try {
            $roleRepo = new RoleRepo();
            $this->validate($request, [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required',
                'phone' => 'required',
                'address' => 'required',
                'type' => 'required',
                'referer' => 'required',
            ]);
            $user = new User();
            $user->fill([
                'username' => $request->get('first_name'),
                'first_name' => $request->get('first_name'),
                'last_name' => $request->get('last_name'),
                'email' => $request->get('email'),
                'referer' => $request->get('referer'),
                'phone' => $request->get('phone'),
                'address' => $request->get('address'),
                'password' => '123456',
                'type' => 3
            ]);
            $user->save();
            $roleRepo->addRole($user->id, Roles::$seller);
            $response = [
                'status' => 'OK',
                'message' => 'Revendedor Guardado'
            ];

            return redirect()->route('users.all.client')->with($response);

        } catch (\Exception $exception) {
            $response = [
                'status' => 'FAILED',
                'message' => 'Error al guardar Revendedor'
            ];
            return redirect()->back()->with($response);
        }
    }

    public function edit($id)
    {
        try {
            $user = User::find($id);

            return view('pages.back.users.edit')->with(['user' => $user]);

        } catch (\Exception $exception) {
            return redirect()->back();
        }
    }

    public function edit_client($id)
    {
        try {
            $user = User::find($id);

            return view('pages.back.users.edit_client')->with(['user' => $user]);

        } catch (\Exception $exception) {
            return redirect()->back();
        }
    }

    public function edit_reseller($id)
    {
        try {
            $user = User::find($id);
            $sellers = User::where('type', 4)->get();
            return view('pages.back.users.edit_reseller')->with(['reseller' => $user, 'sellers' => $sellers]);

        } catch (\Exception $exception) {
            return redirect()->back();
        }
    }


    public function delete($id)
    {
        try {
            $user = User::find($id);
            $user->delete();
            return redirect()->route('users.all');

        } catch (\Exception $exception) {
            return redirect()->back();
        }
    }

    public function delete_client($id)
    {
        try {
            $user = User::find($id);
            $user->delete();
            return redirect()->route('users.all.client');

        } catch (\Exception $exception) {
            return redirect()->back();
        }
    }


    public function update(Request $request, $id)
    {

        try {
            $this->validate($request, [
                'username' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required',
                'phone' => 'required',
                'address' => 'required',
                'type' => 'required',
                'password' => 'confirmed',
            ]);

            $user = User::find($id);

            $user->fill([
                'username' => $request->get('username'),
                'first_name' => $request->get('first_name'),
                'last_name' => $request->get('last_name'),
                'email' => $request->get('email'),
                'phone' => $request->get('phone'),
                'address' => $request->get('address'),
                'type' => $request->get('type')
            ]);

            if (is_null($request->get('password')) || $request->get('password') === '') {

            } else {
                $user->password = $request->get('password');
            }

            $user->save();

            $response = [
                'status' => 'OK',
                'message' => 'Usuario actualizado'
            ];

            return redirect()->route('users.all')->with($response);

        } catch (\Exception $exception) {
            $response = [
                'status' => 'FAILED',
                'message' => 'Error al Actualizar Usuario'
            ];
            return redirect()->back()->with($response);
        }
    }

    public function update_client(Request $request, $id)
    {

        try {
            $this->validate($request, [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required',
                'phone' => 'required',
                'address' => 'required',
            ]);

            $user = User::find($id);

            $user->fill([
                'username' => $request->get('first_name'),
                'first_name' => $request->get('first_name'),
                'last_name' => $request->get('last_name'),
                'email' => $request->get('email'),
                'phone' => $request->get('phone'),
                'address' => $request->get('address'),
            ]);

            $user->save();

            $response = [
                'status' => 'OK',
                'message' => 'Cliente actualizado'
            ];

            return redirect()->route('users.all.client')->with($response);

        } catch (\Exception $exception) {
            $response = [
                'status' => 'FAILED',
                'message' => 'Error al Actualizar Cliente'
            ];
            return redirect()->back()->with($response);
        }
    }

    public function update_reseller(Request $request, $id)
    {

        try {
            $this->validate($request, [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required',
                'phone' => 'required',
                'address' => 'required',
                'referer' => 'required',
            ]);

            $user = User::find($id);

            $user->fill([
                'username' => $request->get('first_name'),
                'first_name' => $request->get('first_name'),
                'last_name' => $request->get('last_name'),
                'email' => $request->get('email'),
                'phone' => $request->get('phone'),
                'referer' => $request->get('referer'),
                'address' => $request->get('address'),
            ]);

            $user->save();

            $response = [
                'status' => 'OK',
                'message' => 'Cliente actualizado'
            ];

            return redirect()->route('users.all.client')->with($response);

        } catch (\Exception $exception) {
            $response = [
                'status' => 'FAILED',
                'message' => 'Error al Actualizar Cliente'
            ];
            return redirect()->back()->with($response);
        }
    }

    public function all()
    {
        try {
            $users = User::where(['type'=> 4])->get();

            foreach ($users as $user) {
                $user->resellers = User::where(['type' => 3, 'referer' => $user->id])->get();
            }

            return view('pages.back.users.all')->with('sellers', $users);

        } catch (\Exception $exception) {
            return redirect()->back();
        }
    }

    public function all_client()
    {
        try {
            $users = User::where(['type' => 3, 'referer' => \Auth::user()->id])->get();

            return view('pages.back.users.all_client')->with('sellers', $users);

        } catch (\Exception $exception) {
            return redirect()->back();
        }
    }
    public function get_resellers(Request $request)
    {
        try {
            $users = User::where(['type' => 3, 'referer' => $request->user])->get();

            return $users;

        } catch (\Exception $exception) {
            return response()->json(['status'=>'FAILED']);
        }
    }

}
