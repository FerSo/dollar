<?php

namespace dollar\Http\Controllers;

use dollar\Security\Enums\Roles;
use dollar\Security\Repositories\RoleRepo;
use Illuminate\Http\Request;

use dollar\User;


class TransactionController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function create()
    {
        return view('pages.back.transactions.add_transaction'); //aca no es necesario colocar el .blade.php
        // view hace referencia a la carpeta recources/views

    }

    public function all()
    {
        return view('pages.back.transactions.all_transaction'); //aca no es necesario colocar el .blade.php
        // view hace referencia a la carpeta recources/views

    }

}
