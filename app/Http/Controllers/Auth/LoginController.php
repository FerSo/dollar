<?php

namespace dollar\Http\Controllers\Auth;


use dollar\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class LoginController extends Controller
{

    public function authenticate(Request $request )
    {
        $this->validate($request, [
            'username' => 'required',
            'password' => 'required'
        ]);

        $username = $request->input('username');
        $password = $request->input('password');
        $credentials = [
            'username' => $username,
            'password' => $password,
        ];

        if (Auth::attempt($credentials)) {

            $user = Auth::user()->id;
            $roles = [];
            $roles[] = Auth::user()->rol;

            $array_permission = [];

            \Session::put('roles',$roles);
            \Session::put('permissions', $array_permission);

            $route = 'index';
            $response = [
                'status' => 'OK',
                'message' => 'Bienvenido ' . \Auth::user()->name
            ];
            session()->flash('message', $response);
            return redirect()->route($route)->with($response);
        }
        $response = [
            'status' => 'FAILED',
            'title' => __('¡Vaya! ¡Algo salió mal!'),
            'message' => __('Credenciales incorrectas')
        ];
        return redirect()->back()->with($response);
    }

    public function login()
    {
        return view('auth.login');
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('login');
    }
}
