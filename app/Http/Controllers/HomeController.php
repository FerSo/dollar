<?php

namespace dollar\Http\Controllers;


use dollar\Bank;
use dollar\BankAccount;
use dollar\tools\Helper;
use dollar\TypeAccount;
use dollar\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index_admin()
    {

        return view('pages.back.index_admin');
    }

    public function settings_views(){
        return view('pages.back.config');
    }
    public function settings_post(Request $request){

        $response = [
            'status' => 'OK',
            'message' => 'Configuración Actualizada'
        ];

        return view('pages.back.config')->with($response);
    }

}
