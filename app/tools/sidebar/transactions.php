<?php

return [
    'Transacion Title' => [
        'id'         => 'transactions.title',
        'icon'       => 'people', // aca puedes cambiar el icono en material icons de google puedes buscarlos
        'url'        => null,
        'permission' => null,
        'url_params' => [],
        'type_user' => 1,
        'slug'       => null,
        'trans'      => __('Transacciones'),
        'subMenu'    => [
            'Transaction add' => [
                'id'         => 'transactions.add',
                'icon'       => 'person_add',
                'url'        => 'transaction.add',
                'permission' => null,
                'url_params' => [],
                'type_user' => 1,
                'slug'       => null,
                'trans'      => __('Agregar transacción'),
                'subMenu'    => [],
            ],
            'Transaction list' => [
                'id'         => 'transactions.list',
                'icon'       => 'person_add',
                'url'        => 'transaction.all',
                'permission' => null,
                'url_params' => [],
                'type_user' => 1,
                'slug'       => null,
                'trans'      => __('Todas las transacciones'),
                'subMenu'    => [],
            ]
        ],
    ],
];