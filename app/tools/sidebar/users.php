<?php

return [
    'Users Title' => [
        'id'         => 'users.title',
        'icon'       => 'people',
        'url'        => null,
        'permission' => null,
        'url_params' => [],
        'type_user' => 1,
        'slug'       => null,
        'trans'      => __('Usuarios'),
        'subMenu'    => [
            'Users add' => [
                'id'         => 'users.add',
                'icon'       => 'person_add',
                'url'        => 'users.add',
                'permission' => null,
                'url_params' => [],
                'type_user' => 1,
                'slug'       => null,
                'trans'      => __('Agregar Usuario'),
                'subMenu'    => [],
            ],
            'Users all' => [
                'id'         => 'users.all',
                'icon'       => 'menu',
                'url'        => 'users.all',
                'permission' => null,
                'url_params' => [],
                'type_user' => 1,
                'slug'       => null,
                'trans'      => __('Todos los Usuarios'),
                'subMenu'    => [],
            ]
        ],
    ],
];