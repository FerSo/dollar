<?php
namespace dollar\tools\sidebar;

use dollar\tools\Helper;

class items extends \dollar\Http\Controllers\Controller
{
    public static function sidebar()
    {
        return array_merge(
            Helper::sidebar('dashboard'),
            Helper::sidebar('users'),
            Helper::sidebar('accounts'),
            Helper::sidebar('transactions'),
            Helper::sidebar('settings')
        );
    }
}
