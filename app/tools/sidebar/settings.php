<?php

    return [
        'Setting Title' => [
            'id'         => 'settings.title',
            'icon'       => 'home',
            'url'        => 'settings',
            'url_params' => [],
            'permission' => null,
            'type_user' => null,
            'slug'       => null,
            'trans'      => __('Configuración'),
            'subMenu'    => [],
        ]
    ];
