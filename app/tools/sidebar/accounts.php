<?php

return [
    'Accounts Title' => [
        'id'         => 'accounts.title',
        'icon'       => 'account_balance',
        'url'        => null,
        'permission' => null,
        'url_params' => [],
        'type_user' => 1,
        'slug'       => null,
        'trans'      => __('Cuentas'),
        'subMenu'    => [
            'Accounts add' => [
                'id'         => 'accounts.add',
                'icon'       => 'add',
                'url'        => 'accounts.add',
                'permission' => null,
                'url_params' => [],
                'type_user' => 1,
                'slug'       => null,
                'trans'      => __('Agregar Cuenta'),
                'subMenu'    => [],
            ],
            'Accounts all' => [
                'id'         => 'accounts.all',
                'icon'       => 'menu',
                'url'        => 'accounts.all',
                'permission' => null,
                'url_params' => [],
                'type_user' => 1,
                'slug'       => null,
                'trans'      => __('Mis Cuentas'),
                'subMenu'    => [],
            ],
        ],
    ],
];