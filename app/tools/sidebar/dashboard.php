<?php

    return [
        'Principal Admin Title' => [
            'id'         => 'principal.admin.title',
            'icon'       => 'home',
            'url'        => 'index',
            'url_params' => [],
            'permission' => null,
            'type_user' => null,
            'slug'       => null,
            'trans'      => __('Principal'),
            'subMenu'    => [],
        ],
    ];
