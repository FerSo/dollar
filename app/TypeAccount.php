<?php

namespace dollar;

use Illuminate\Database\Eloquent\Model;

class TypeAccount extends Model
{

    protected $table = 'type_account';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        'id',
        'name',
    ];

}
