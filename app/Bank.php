<?php

namespace dollar;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{

    protected $table = 'banks';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        'id',
        'name',
        'logo',
        'prefix',
    ];

}
