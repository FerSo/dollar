<?php

namespace dollar;


use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    protected $table = 'users';

    protected $primaryKey = 'id';


    protected $fillable = [
        'id',
        'name',
        'username',
        'rol',
        'type',
        'comission',
        'balance',
    ];


    protected $hidden = ['password', 'remember_token'];


    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }
}
