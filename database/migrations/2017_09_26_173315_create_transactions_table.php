<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('type')->unsigned(); // ingreso o egreso
            $table->integer('account')->unsigned(); // cuenta bancaria destino u origen
            $table->integer('bank')->unsigned()->nullable(); // cuenta bancaria destino u origen
            $table->float('amount',12,2);
            $table->enum('currency_extern',['USD','PEN']); //dolar o peso
            $table->text('observation')->nullable();
            $table->integer('vendor')->unsigned();
            $table->float('amount_extern')->nullable(); //total de dolares (si es el caso de compra de dolares)
            $table->float('exchange')->nullable(); //cambio en bolivares
            $table->timestamps();
            $table->foreign('vendor')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('bank')->references('id')->on('banks')->onDelete('cascade');
            $table->foreign('account')->references('id')->on('bank_account')->onDelete('cascade');
            $table->foreign('type')->references('id')->on('type_transaction')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
