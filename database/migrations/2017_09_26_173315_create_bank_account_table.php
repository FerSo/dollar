<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankAccountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_account', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bank')->unsigned();
            $table->string('dni')->nullable();
            $table->string('number');
            $table->string('owner')->nullable();
            $table->float('balance',12,2);
            $table->integer('type')->unsigned();
            $table->foreign('bank')->references('id')->on('banks')->onDelete('cascade');
            $table->foreign('type')->references('id')->on('type_account')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_account');
    }
}
