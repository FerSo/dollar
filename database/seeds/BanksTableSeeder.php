<?php

use Illuminate\Database\Seeder;
use dollar\User;
use dollar\Security\Enums\Roles;
use dollar\Security\Repositories\RoleRepo;

class BanksTableSeeder extends Seeder
{

    public function banks()
    {

        $json = '{
        "RECORDS":[{
"id":"VE-0102",
"description":"Banco de Venezuela",
"prefix":"0102",
"country":"VE",
"logo":null
},
{
"id":"VE-0104",
"description":"Venezolano de Crédito",
"prefix":"0104",
"country":"VE",
"logo":null
},
{
"id":"VE-0105",
"description":"Banco Mercantil",
"prefix":"0105",
"country":"VE",
"logo":null
},
{
"id":"VE-0108",
"description":"Banco Provincial",
"prefix":"0108",
"country":"VE",
"logo":null
},
{
"id":"VE-0114",
"description":"Bancaribe",
"prefix":"0114",
"country":"VE",
"logo":null
},
{
"id":"VE-0115",
"description":"Banco Exterior",
"prefix":"0115",
"country":"VE",
"logo":null
},
{
"id":"VE-0116",
"description":"Banco Occidental de Descuento",
"prefix":"0116",
"country":"VE",
"logo":null
},
{
"id":"VE-0128",
"description":"Banco Caroní C.A. Banco Universal",
"prefix":"0128",
"country":"VE",
"logo":null
},
{
"id":"VE-0134",
"description":"Banesco Banco Universal",
"prefix":"0134",
"country":"VE",
"logo":null
},
{
"id":"VE-0137",
"description":"Banco Sofitasa",
"prefix":"0137",
"country":"VE",
"logo":null
},
{
"id":"VE-0138",
"description":"Banco Plaza Banco Universal",
"prefix":"0138",
"country":"VE",
"logo":null
},
{
"id":"VE-0146",
"description":"Banco de la Gente Emprendedora",
"prefix":"0146",
"country":"VE",
"logo":null
},
{
"id":"VE-0149",
"description":"Banco del Pueblo Soberano",
"prefix":"0149",
"country":"VE",
"logo":null
},
{
"id":"VE-0151",
"description":"BFC Banco Fondo Común C.A Banco Universal",
"prefix":"0151",
"country":"VE",
"logo":null
},
{
"id":"VE-0156",
"description":"100% Banco",
"prefix":"0156",
"country":"VE",
"logo":null
},
{
"id":"VE-0157",
"description":"DelSur Banco Universal",
"prefix":"0157",
"country":"VE",
"logo":null
},
{
"id":"VE-0163",
"description":"Banco del Tesoro",
"prefix":"0163",
"country":"VE",
"logo":null
},
{
"id":"VE-0166",
"description":"Banco Agrícola de Venezuela",
"prefix":"0166",
"country":"VE",
"logo":null
},
{
"id":"VE-0168",
"description":"Bancrecer, S.A. Banco Microfinanciero",
"prefix":"0168",
"country":"VE",
"logo":null
},
{
"id":"VE-0169",
"description":"Mi Banco Banco Microfinanciero",
"prefix":"0169",
"country":"VE",
"logo":null
},
{
"id":"VE-0171",
"description":"Banco Activo",
"prefix":"0171",
"country":"VE",
"logo":null
},
{
"id":"VE-0172",
"description":"Bancamiga Banco Microfinanciero",
"prefix":"0172",
"country":"VE",
"logo":null
},
{
"id":"VE-0173",
"description":"Banco Internacional de Desarrollo",
"prefix":"0173",
"country":"VE",
"logo":null
},
{
"id":"VE-0174",
"description":"Banplus Banco Universal",
"prefix":"0174",
"country":"VE",
"logo":null
},
{
"id":"VE-0175",
"description":"Banco Bicentenario",
"prefix":"0175",
"country":"VE",
"logo":null
},
{
"id":"VE-0176",
"description":"Banco Espirito Santo, S.A. Sucursal Venezuela B.U.",
"prefix":"0176",
"country":"VE",
"logo":null
},
{
"id":"VE-0177",
"description":"Banco de la Fuerza Armada Nacional Bolivariana, B.U.",
"prefix":"0177",
"country":"VE",
"logo":null
},
{
"id":"VE-0190",
"description":"Citibank N.A",
"prefix":"0190",
"country":"VE",
"logo":null
},
{
"id":"VE-0191",
"description":"Banco Nacional de Crédito",
"prefix":"0191",
"country":"VE",
"logo":null
},
{
"id":"VE-0601",
"description":"Instituto Municipal de Crédito Popular",
"prefix":"0601",
"country":"VE",
"logo":null
}
]
}';
        $Banks = json_decode($json);

        return $Banks->RECORDS;

    }

    public function run()
    {
        foreach ($this->banks() as $item => $value) {
            $Bank = new \dollar\Bank();
            $Bank->name = $value->description;
            $Bank->prefix = $value->prefix;
            $Bank->logo = $value->logo;
            $Bank->save();
        }
    }
}
