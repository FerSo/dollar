<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \dollar\Security\Entities\Role::create([
            'name' => 'Admin'
        ]);

        \dollar\Security\Entities\Role::create([
            'name' => 'Seller'
        ]);

    }
}
