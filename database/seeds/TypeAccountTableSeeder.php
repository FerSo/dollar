<?php

use Illuminate\Database\Seeder;
use dollar\User;
use dollar\Security\Enums\Roles;
use dollar\Security\Repositories\RoleRepo;

class TypeAccountTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data_save = [
            'checking',
            'saving'
        ];
        foreach ($data_save as $item){
            $code = new \dollar\TypeAccount();
            $code->name = $item;
            $code->save();
        }

    }
}
