<?php

use Illuminate\Database\Seeder;
use dollar\User;
use dollar\Security\Enums\Roles;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'username' => 'admin',
            'password' => '12345678',
            'name' => 'Administrador',
            'rol' => Roles::$admin,
        ]);
    }
}
