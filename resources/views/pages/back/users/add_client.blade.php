@extends('layouts.app')
@section('title') Agregar Revendedor @endsection

@section('users.title', 'active')
@section('users.add.client', 'active')


@section('styles')

@endsection
@section('content')
    <div class="block-header">
        <h2>Revendedores</h2>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Nuevo Revendedor
                        <small></small>
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);" class=" waves-effect waves-block">Action</a></li>
                                <li><a href="javascript:void(0);" class=" waves-effect waves-block">Another action</a>
                                </li>
                                <li><a href="javascript:void(0);" class=" waves-effect waves-block">Something else
                                        here</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <div class="row clearfix">
                        <form action="{{route('users.add.client.post')}}" method="post">
                            <div class="from-group">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="first_name" required name="first_name"
                                                   class="form-control" value="" placeholder="Primer Nombre">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="last_name" required name="last_name"
                                                   class="form-control" value="" placeholder="Primer Apellido">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="email" id="email" required name="email" class="form-control"
                                                   value="" placeholder="Correo Electrónico">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="phone" required name="phone" class="form-control"
                                                   value="" placeholder="Telefono">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="address" required name="address" class="form-control"
                                                   value="" placeholder="Dirección">
                                        </div>
                                    </div>
                                </div>
                                <input name="type" value="4" type="hidden">
                            </div>
                            <br>
                            <div class="col-md-2 pull-right" style="margin-top:5px">

                                <button class="btn btn-primary"> Guardar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')


@endsection
