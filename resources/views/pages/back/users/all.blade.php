@extends('layouts.app')
@section('title') Todos los Usuarios @endsection
@section('users.title', 'active')
@section('users.all', 'active')

@section('styles')

@endsection
@section('content')
    <div class="block-header">
        <h2>Vendedores</h2>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Todos Los Vendedores Registrados
                        <small></small>
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);" class=" waves-effect waves-block">Action</a></li>
                                <li><a href="javascript:void(0);" class=" waves-effect waves-block">Another action</a></li>
                                <li><a href="javascript:void(0);" class=" waves-effect waves-block">Something else here</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <div class="table-responsive"><table class="ui celled padded table">
                            <thead>
                            <tr>
                                <td>ID</td>
                                <td>Nombre</td>
                                <td></td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($sellers as $seller)
                                <tr>
                                    <td>{{$seller->id}}</td>
                                    <td>{{$seller->first_name}}</td>
                                    <td>
                                        <a class="btn btn-success modal_resellers" data-id="{{$seller->id}}" data-name="{{$seller->first_name}} {{$seller->last_name}}" data-toggle="modal" data-target="#seeResellers">Ver Revendedores</a>
                                        <a href="{{route('users.edit',$seller->id)}}" class="btn btn-primary ">Editar</a>
                                        <a href="{{route('users.delete',$seller->id)}}" class="btn btn-danger ">Eliminar</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="seeResellers" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="seeOfferLabel">Revendedores de <span class="seller_name"></span></h4>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="ui celled padded table">
                            <thead>
                            <tr>
                                <td>ID</td>
                                <td>Nombre</td>
                                <td></td>
                            </tr>
                            </thead>
                            <tbody class="table_resellers_body">

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn close_details btn-link waves-effect" data-dismiss="modal">Cerrar
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).on('click','.modal_resellers',function () {
            var name = $(this).data('name');
            var id = $(this).data('id');
            $.get('{{route('get.resellers')}}?user='+id).success(function (data) {
                $('.table_resellers_body').html('');
                $('.seller_name').text(name);

                $.each(data,function (i,v) {
                    $('.table_resellers_body').append('<tr>' +
                        '<td>'+v.id+'</td>' +
                        '<td>'+v.first_name+'</td>' +
                        '<td>' +
                        '<a href="/users/edit/reseller/'+v.id+'" class="btn btn-primary ">Editar</a>' +
                        '<a href="/users/delete/reseller/'+v.id+'" class="btn btn-danger ">Eliminar</a>' +
                        '</td>' +
                        '</tr>');
                });
            });
        });
    </script>
@endsection
