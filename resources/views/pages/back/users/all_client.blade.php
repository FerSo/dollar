@extends('layouts.app')
@section('title') Todos mis Clientes @endsection
@section('users.title', 'active')
@section('users.all.client', 'active')

@section('styles')

@endsection
@section('content')
    <div class="block-header">
        <h2>Clientes</h2>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Todos mis Clientes
                        <small></small>
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);" class=" waves-effect waves-block">Action</a></li>
                                <li><a href="javascript:void(0);" class=" waves-effect waves-block">Another action</a></li>
                                <li><a href="javascript:void(0);" class=" waves-effect waves-block">Something else here</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <div class="table-responsive"><table class="ui celled padded table">
                            <thead>
                            <tr>
                                <td>ID</td>
                                <td>Nombre</td>
                                <td></td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($sellers as $seller)
                                <tr>
                                    <td>{{$seller->id}}</td>
                                    <td>{{$seller->first_name}}</td>
                                    <td><a href="{{route('users.edit.client',$seller->id)}}" class="btn btn-primary ">Editar</a>
                                        <a href="{{route('users.delete.client',$seller->id)}}" class="btn btn-danger ">Eliminar</a></td>
                                    <td>

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table></div>

                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')


@endsection
