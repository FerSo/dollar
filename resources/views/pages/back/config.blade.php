@extends('layouts.app')
@section('title') Editar Configuracíon @endsection

@section('settings.title', 'active')



@section('styles')

@endsection
@section('content')
    <div class="block-header">
        <h2>Vendedores</h2>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Editar Configuración
                        <small></small>
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);" class=" waves-effect waves-block">Action</a></li>
                                <li><a href="javascript:void(0);" class=" waves-effect waves-block">Another action</a>
                                </li>
                                <li><a href="javascript:void(0);" class=" waves-effect waves-block">Something else
                                        here</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <div class="row clearfix">
                        <form action="{{route('settings.edit.post')}}" method="post">
                            <div class="col-md-12">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="minime"> Monto Minimo en Dolares</label>
                                        <div class="form-line">
                                            <input type="text" id="minime" required name="minime"
                                                   class="form-control"
                                                   value="{{\dollar\tools\Helper::number( \Illuminate\Support\Facades\Config::get("settings.minime"),2)}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="dollarminus"> Valor del Dia (Dolar) < 500$ </label>
                                            <div class="form-line">
                                                <input type="text" id="dollarminus" required name="dollarminus"
                                                       class="form-control"
                                                       value="{{\Illuminate\Support\Facades\Config::get("settings.dollar.minus")}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="dollarplus"> Valor del Dia (Dolar) > 500$ </label>
                                            <div class="form-line">
                                                <input type="text" id="dollarplus" required name="dollarplus"
                                                       class="form-control"
                                                       value="{{\Illuminate\Support\Facades\Config::get("settings.dollar.plus")}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="eurominus"> Valor del Dia (Euro) < 500 €</label>
                                            <div class="form-line">
                                                <input type="text" id="eurominus" required name="eurominus"
                                                       class="form-control"
                                                       value="{{\Illuminate\Support\Facades\Config::get("settings.euro.minus")}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="europlus"> Valor del Dia (Euro) > 500 €</label>
                                            <div class="form-line">
                                                <input type="text" id="europlus" required name="europlus"
                                                       class="form-control"
                                                       value="{{\Illuminate\Support\Facades\Config::get("settings.euro.plus")}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>

                            <br>
                            <div class="col-md-2 pull-right" style="margin-top:5px">

                                <button class="btn btn-primary"> Guardar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection
