@extends('layouts.app')
@section('title') Agregar Cuenta Bancaria @endsection

@section('accounts.title', 'active')
@section('accounts.add', 'active')

@section('styles')

@endsection
@section('content')
    <div class="block-header">
        <h2>Cuentas Bancarias</h2>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Agregar Cuenta Bancaria
                        <small></small>
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);" class=" waves-effect waves-block">Action</a></li>
                                <li><a href="javascript:void(0);" class=" waves-effect waves-block">Another action</a>
                                </li>
                                <li><a href="javascript:void(0);" class=" waves-effect waves-block">Something else
                                        here</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <div class="row clearfix">
                        <form action="{{route('accounts.add.post')}}" method="post">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="beneficiary" required name="beneficiary"
                                               class="form-control" value="" placeholder="Nombre del Beneficiario * ">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">

                                <select id="bank" required class="form-control" name="bank">
                                    <option value="">Seleccione un Banco *</option>
                                    @foreach($banks as $bank)
                                        <option value="{{$bank->id}}">{{$bank->name}}</option>
                                    @endforeach
                                </select>

                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="number" id="number" required name="number"
                                               class="form-control" value="" placeholder="Numero de cuenta * ">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">

                                <select id="type" class="form-control" required name="type">
                                    <option value="">Seleccione un tipo de cuenta *</option>
                                    <option value="1">Checking / Corriente</option>
                                    <option value="2">Saving / Ahorros</option>
                                    @if(\Auth::user()->type === 2)
                                        <option value="3">View / Vista</option>
                                    @endif
                                </select>

                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="email" id="email" required name="email"
                                               class="form-control" value="" placeholder="Correo Electronico">
                                    </div>
                                </div>
                            </div>
                            @if(\Auth::user()->type !== 2)
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="dni" required name="dni"
                                                   class="form-control" value="" placeholder="DNI / CEDULA">
                                        </div>
                                    </div>
                                </div>
                                @else
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="aba" name="aba" class="form-control"
                                                   value="" placeholder="ABA">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="swift" name="swift" class="form-control"
                                                   value="" placeholder="SWIFT">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="iban" name="iban" class="form-control"
                                                   value="" placeholder="IBAN">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="routing" name="routing" class="form-control"
                                                   value="" placeholder="ROUTING">
                                        </div>
                                    </div>
                                </div>
                            @endif


                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="address"
                                               name="address" class="form-control" value="" placeholder="Dirección">
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="col-md-2 pull-right" style="margin-top:5px">

                                <button class="btn btn-primary"> Guardar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('#type').selectpicker();
        $('#bank').selectpicker();
    </script>

@endsection
