@extends('layouts.app')
@section('title') Todas Las Cuentas Bancarias @endsection
@section('accounts.title', 'active')
@section('accounts.all', 'active')

@section('styles')

@endsection
@section('content')
    <div class="block-header">
        <h2>Cuentas Bancarias</h2>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Todas Las Cuentas Bancarias
                        <small></small>
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);" class=" waves-effect waves-block">Action</a></li>
                                <li><a href="javascript:void(0);" class=" waves-effect waves-block">Another action</a>
                                </li>
                                <li><a href="javascript:void(0);" class=" waves-effect waves-block">Something else
                                        here</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <table class="ui celled padded table">
                        <thead>
                        <tr>
                            <td>Banco</td>
                            <td>Numero de cuenta</td>
                            <td></td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($accounts as $account)
                            <tr>
                                <td>{{$account->bank_name}}</td>
                                <td>{{$account->number}}</td>
                                <td><a href="{{route('accounts.edit',$account->id)}}" class="btn btn-primary">Editar</a>
                                    <a href="{{route('accounts.delete',$account->id)}}" class="btn btn-danger">Eliminar</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')


@endsection
