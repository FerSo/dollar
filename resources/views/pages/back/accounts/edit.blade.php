@extends('layouts.app')
@section('title') Editar Cuenta Bancaria @endsection

@section('accounts.title', 'active')

@section('styles')

@endsection
@section('content')
    <div class="block-header">
        <h2>Cuentas Bancarias</h2>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Editar Cuenta Bancaria
                        <small></small>
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);" class=" waves-effect waves-block">Action</a></li>
                                <li><a href="javascript:void(0);" class=" waves-effect waves-block">Another action</a>
                                </li>
                                <li><a href="javascript:void(0);" class=" waves-effect waves-block">Something else
                                        here</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <div class="row clearfix">
                        <form action="{{route('accounts.edit.post',[$account->id])}}" method="post">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="beneficiary" required name="beneficiary"
                                               class="form-control" value="{{$account->beneficiary}}" placeholder="Nombre del Beneficiario * ">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">

                                <select id="bank" required class="form-control" name="bank">
                                    <option value="">Seleccione un Banco *</option>
                                    @foreach($banks as $bank)
                                        <option value="{{$bank->id}}" {{($account->bank === $bank->id ? 'selected' : '')}}>{{$bank->name}}</option>
                                    @endforeach
                                </select>

                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="number" id="number" required name="number"
                                               class="form-control" value="{{$account->number}}" placeholder="Numero de cuenta * ">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">

                                <select id="type" class="form-control" required name="type">
                                    <option value="">Seleccione un tipo de cuenta *</option>
                                    <option value="1"  {{($account->type === 1 ? 'selected' : '')}}>Checking / Corriente</option>
                                    <option value="2"  {{($account->type === 2 ? 'selected' : '')}}>Saving / Ahorros</option>
                                    <option value="3"  {{($account->type === 3 ? 'selected' : '')}}>View / Vista</option>
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="email" id="email" name="email"
                                               class="form-control" value="{{$account->email}}" placeholder="Correo Electronico *">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="dni" name="dni"
                                               class="form-control" value="{{$account->dni}}" placeholder="DNI / CEDULA">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="aba"  name="aba" class="form-control"
                                               value="{{$account->aba}}" placeholder="ABA">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="swift"  name="swift" class="form-control"
                                               value="{{$account->swift}}" placeholder="SWIFT">
                                        <label for="swift" class="form-label"></label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="iban"  name="iban" class="form-control"
                                               value="{{$account->iban}}" placeholder="IBAN">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="routing" name="routing" class="form-control"
                                               value="{{$account->routing}}" placeholder="ROUTING">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" id="address"
                                               name="address" class="form-control" value="{{$account->address}}" placeholder="Dirección">
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="col-md-2 pull-right" style="margin-top:5px">

                                <button class="btn btn-primary"> Guardar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('#type').selectpicker();
        $('#bank').selectpicker();
    </script>

@endsection
