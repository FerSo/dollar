@extends('layouts.app')
@section('title') Nueva Transacción @endsection

@section('transactions.title', 'active')
@section('transactions.add', 'active')

@section('styles')

@endsection
@section('content')
    <div class="block-header">
        <h2>Nueva transacción</h2>
    </div>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>Datos de la Transferencia</h2>
                    </div>
                    <div class="body">
                        <form id="form_validation" method="POST" novalidate="novalidate">
                            <div class="row clearfix">
                                <div class="col-md-2">
                                    <div class="form-group form-float">
                                        <div class="form-line ">
                                            <input type="text" class="form-control" name="cod_vendedor" required="" aria-required="true">
                                            <label class="form-label">Codigo Vendedor</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group form-float">
                                        <div class="form-line ">
                                            <input type="text" class="form-control" name="banco_emisor" required="" aria-required="true">
                                            <label class="form-label">Banco Emisor</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group form-float">
                                        <div class="form-line ">
                                            <input type="text" class="form-control" name="banco_receptor" required="" aria-required="true">
                                            <label class="form-label">Banco Receptor</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row clearfix">
                                <div class="col-md-12">
                                    <div class="form-group form-float">
                                        <div class="form-line ">
                                            <input type="text" class="form-control" name="titular_banco_receptor" required="" aria-required="true">
                                            <label class="form-label">Titular Banco Receptor</label>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="row clearfix">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <p>Moneda</p>
                                        <input type="radio" name="moneda" id="dolar" class="with-gap">
                                        <label for="dolar">Dolar</label>

                                        <input type="radio" name="moneda" id="soles" class="with-gap">
                                        <label for="soles" class="m-l-20">Soles</label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <b>Fecha</b>
                                    <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">date_range</i>
                                            </span>
                                        <div class="form-line">
                                            <input type="text" class="form-control date" placeholder="Ex: 30/07/2016">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <b>Monto Enviado</b>
                                    <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">attach_money</i>
                                            </span>
                                        <div class="form-line">
                                            <input type="text" class="form-control money-dollar" placeholder="Ex: 99,99 $">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <b>Monto Recibido</b>
                                    <div class="input-group">
                                        <div class="form-line">
                                            <input type="text" class="form-control money-euro" placeholder="Ex: 30000,00 Bs">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-12">
                                    <div class="form-group form-float">
                                        <div class="form-line ">
                                            <input type="text" class="form-control" name="tipo_pago" required="" aria-required="true">
                                            <label class="form-label">Tipo pago Recibido</label>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <button class="btn btn-primary waves-effect" type="submit">Registrar</button>
                        </form>
                    </div>
                </div>
            </div>

    </div>

@endsection
@section('scripts')
@endsection
