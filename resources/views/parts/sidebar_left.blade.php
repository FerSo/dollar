<aside id="leftsidebar" class="sidebar">
    <div class="user-info">
        {{--<div class="image">--}}
            {{--<img src="{{(\Illuminate\Support\Facades\Auth::user()->image === '' ? 'images/user.png': 'images/user.png')}}"--}}
                 {{--width="48" height="48" alt="User"/>--}}
        {{--</div>--}}
        <div class="info-container">
            <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> {{\Auth::user()->name}}</div>
            <div class="btn-group user-helper-dropdown">
                <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                <ul class="dropdown-menu pull-right">
                    <li><a href="{{route('logout')}}"><i class="material-icons">input</i>Cerrar Sesión</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="menu">
        <ul class="list">
            @php($permissions = session()->get('permissions'))
            @foreach(\dollar\tools\sidebar\items::sidebar() as $itemIndex => $item)
                @if(isset($permissions[$item['permission']]) || is_null($item['permission']))
                    <li class="@yield($item['id'])">
                        <a href="{{(!is_null($item['url']) ? route($item['url']) : 'javascript:void(0);')}}" class="{{(count($item['subMenu']) > 0 ? 'menu-toggle' : '')}}">
                            <i class="material-icons">{{$item['icon']}}</i>
                            <span>{{$item['trans']}}</span>
                        </a>
                        @if(count($item['subMenu']) > 0)
                            <ul class="ml-menu">
                                @foreach($item['subMenu'] as $itemIndex1 => $item1)
                                    @if(isset($permissions[$item1['permission']])  ||  is_null($item1['permission']))
                                        <li class="@yield($item1['id'])">
                                            <a href="{{(!is_null($item1['url']) ? route($item1['url']) : 'javascript:void(0);')}}" class="{{(count($item1['subMenu']) > 0 ? 'menu-toggle' : '')}}">
                                                <i class="material-icons">{{$item1['icon']}}</i>
                                                <span>{{$item1['trans']}}</span>
                                            </a>
                                            @if(count($item1['subMenu']) > 0)
                                                <ul class="ml-menu">
                                                    @foreach($item1['subMenu'] as $itemIndex2 => $item2)
                                                        @if(isset($permissions[$item2['permission']]) ||  is_null($item2['permission']))
                                                            <li>
                                                                <a href="{{(!is_null($item2['url']) ? route($item2['url']) : 'javascript:void(0);')}}" class="{{(count($item2['subMenu']) > 0 ? 'menu-toggle' : '')}}">
                                                                    <i class="material-icons">{{$item2['icon']}}</i>
                                                                    <span>{{$item2['trans']}}</span>
                                                                </a>
                                                                @if(count($item2['subMenu']) > 0)
                                                                    <ul class="ml-menu">
                                                                        @foreach($item2['subMenu'] as $itemIndex3 => $item3)
                                                                            @if(isset($permissions[$item3['permission']])  ||  is_null($item3['permission']))
                                                                                <li>
                                                                                    <a href="{{(!is_null($item3['url']) ? route($item3['url']) : 'javascript:void(0);')}}">
                                                                                        <i class="material-icons">{{$item3['icon']}}</i>
                                                                                        <span>{{$item3['trans']}}</span>
                                                                                    </a>
                                                                                </li>
                                                                            @endif
                                                                        @endforeach
                                                                    </ul>
                                                                @endif
                                                            </li>
                                                        @endif
                                                    @endforeach
                                                </ul>
                                            @endif
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        @endif
                    </li>
                @endif
            @endforeach
        </ul>
    </div>
    <!-- #Menu -->
    <!-- Footer -->
    <div class="legal">
        {{--<div class="copyright">--}}
            {{--&copy; 2016 - 2017 <a href="javascript:void(0);">AdminBSB - Material Design</a>.--}}
        {{--</div>--}}
        {{--<div class="version">--}}
            {{--<b>Version: </b> 1.0.5--}}
        {{--</div>--}}
    </div>
    <!-- #Footer -->
</aside>