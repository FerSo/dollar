<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Sign In | Bootstrap Based Admin Template - Material Design</title>
    <!-- Favicon-->
    <link rel="icon" href="../../favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->

    <link href="{{asset('plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{asset('plugins/node-waves/waves.css')}}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{asset('plugins/animate-css/animate.css')}}" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <style>
        [type="checkbox"].filled-in:not(:checked) + label:after {

        }
    </style>
    <script>

    </script>
</head>

<body class="login-page" onload="">
<div class="page-loader-wrapper lo" style="display: block">
    <div class="loader">
        <div class="preloader">
            <div class="spinner-layer pl-red">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
        <p>Please wait...</p>
    </div>
</div>
<div class="login-box">
    <div class="logo">
        <a href="javascript:void(0);"><b>Verdes</b></a>
        {{--<small>Sistema de Compra y venta de Dolares</small>--}}
    </div>
    <div class="card">
        <div class="body">
            <form id="sign_in" action="{{ route('login') }}" method="POST">
                <div class="msg">Ingresa tus Credenciales para Iniciar Sesión</div>
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                    <div class="form-line">
                        <input type="text" class="form-control" name="username" placeholder="Username" required autofocus>
                    </div>
                </div>
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                    <div class="form-line">
                        <input type="password" class="form-control" name="password" placeholder="Password" required>
                    </div>
                </div>
                <div class="row">
                        <button class="btn btn-block bg-pink waves-effect init" type="submit">Iniciar Sesión</button>
                </div>
                {{--<div class="row m-t-15 m-b--20">--}}
                    {{--<div class="col-xs-6">--}}
                        {{--<a href="sign-up.html">Register Now!</a>--}}
                    {{--</div>--}}
                    {{--<div class="col-xs-6 align-right">--}}
                        {{--<a href="forgot-password.html">Forgot Password?</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </form>
        </div>
    </div>

</div>

<!-- Jquery Core Js -->
<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>

<!-- Bootstrap Core Js -->
<script src="{{asset('plugins/bootstrap/js/bootstrap.js')}}"></script>

<!-- Waves Effect Plugin Js -->
<script src="{{asset('plugins/node-waves/waves.js')}}"></script>

<!-- Validation Plugin Js -->
<script src="{{asset('plugins/jquery-validation/jquery.validate.js')}}"></script>
<script src="{{asset('plugins/bootstrap-notify/bootstrap-notify.js')}}"></script>


<!-- Custom Js -->
<script src="{{asset('js/admin.js')}}"></script>
<script>
    $('#sign_in').on('submit',function () {
        $('.lo').show();
    })
</script>
@if(count($errors) > 0)
    <script>
        $(function () {
            @foreach($errors->all() as $error)
              $.notify({ message: '{{ $error }}'},{type: 'danger'});
            @endforeach
        });
    </script>
@endif

@if(session()->has('status'))
    @if (session()->get('status') == 'OK')
        @if(is_array(session()->get('message')))
            @foreach(session()->get('message') as $item)
                <script>
                    $(function () {
                        $.notify({ message: '{!! session()->get('message') !!}'},{type: 'success'});
                    });
                </script>
            @endforeach
        @else
            <script>
                $(function () {
                    $.notify({ message: '{!! session()->get('message') !!}'},{type: 'success'});
                });
            </script>
        @endif
    @endif

    @if (session()->get('status') == 'FAILED')

        @if(is_array(session()->get('message')))
            @foreach(session()->get('message') as $item)
                <script>
                    $(function () {
                        $.notify({ message: '{!! $item[0] !!}'},{type: 'danger'});
                    });
                </script>
            @endforeach
        @else
            <script>
                $(function () {
                    $.notify({ message: '{!! session()->get('message') !!}'},{type: 'danger'});
                });
            </script>
        @endif
    @endif
@endif

</body>

</html>