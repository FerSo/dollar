<?php

\Route::group([
    'middleware' => ['auth'],
    'prefix' => 'users'
    ], function() {

    Route::get('add', [
        'as' => 'users.add',
        'uses' => 'UserController@create',
    ]);
    Route::post('add', [
        'as' => 'users.add.post',
        'uses' => 'UserController@store',
    ]);
    Route::get('edit/{id}', [
        'as' => 'users.edit',
        'uses' => 'UserController@edit',
    ]);
    Route::get('delete/{id}', [
        'as' => 'users.delete',
        'uses' => 'UserController@delete',
    ]);
    Route::post('edit/{id}', [
        'as' => 'users.edit.post',
        'uses' => 'UserController@update',
    ]);
    Route::get('', [
        'as' => 'users.all',
        'uses' => 'UserController@all',
    ]);



    Route::get('new/client', [
        'as' => 'users.add.client',
        'uses' => 'UserController@new_client',
    ]);
    Route::post('add/client', [
        'as' => 'users.add.client.post',
        'uses' => 'UserController@store_client',
    ]);

    Route::get('all/client', [
        'as' => 'users.all.client',
        'uses' => 'UserController@all_client',
    ]);

    Route::get('delete/client/{id}', [
        'as' => 'users.delete.client',
        'uses' => 'UserController@delete_client',
    ]);


    Route::get('edit/client/{id}', [
        'as' => 'users.edit.client',
        'uses' => 'UserController@edit_client',
    ]);

    Route::post('edit/client/{id}', [
        'as' => 'users.edit.post.client',
        'uses' => 'UserController@update_client',
    ]);






    Route::get('ger/resellers', [
        'as' => 'get.resellers',
        'uses' => 'UserController@get_resellers',
    ]);

    Route::get('new/reseller', [
        'as' => 'users.add.reseller',
        'uses' => 'UserController@new_reseller',
    ]);
    Route::post('add/reseller', [
        'as' => 'users.add.reseller.post',
        'uses' => 'UserController@store_reseller',
    ]);

    Route::get('delete/reseller/{id}', [
        'as' => 'users.delete.reseller',
        'uses' => 'UserController@delete_client',
    ]);


    Route::get('edit/reseller/{id}', [
        'as' => 'users.edit.reseller',
        'uses' => 'UserController@edit_reseller',
    ]);

    Route::post('edit/reseller/{id}', [
        'as' => 'users.edit.reseller.post',
        'uses' => 'UserController@update_reseller',
    ]);
});
