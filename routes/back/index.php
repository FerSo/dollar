<?php

\Route::group([
    'middleware' => ['auth'],
    'prefix' => ''
], function () {
    Route::get('/', [
        'as' => 'index',
        'uses' => 'HomeController@index_admin',
    ]);

    Route::get('/settings', [
        'as' => 'settings',
        'uses' => 'HomeController@settings_views',
    ]);
    Route::post('/settings', [
        'as' => 'settings.edit.post',
        'uses' => 'HomeController@settings_post',
    ]);

});
