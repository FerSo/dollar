<?php

\Route::group([
    'middleware' => ['auth'],
    'prefix' => 'transaction'
    ], function() {

    Route::get('add', [
        'as' => 'transaction.add',
        'uses' => 'TransactionController@create',
    ]);

    Route::get('all', [
        'as' => 'transaction.all',
        'uses' => 'TransactionController@all',
    ]);

});
