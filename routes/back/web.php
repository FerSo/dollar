<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




/**
 * Auth routes
 */
\Route::group(['middleware' => ['guest']], function() {

    Route::get('login', [
        'as' => 'login',
        'uses' => 'Auth\LoginController@login',
    ]);

    Route::post('login', [
        'as' => 'login.post',
        'uses' => 'Auth\LoginController@authenticate',
    ]);
});

/**
 * Auth routes
 */
\Route::group(['middleware' => ['auth']], function() {
    Route::get('logout', [
        'as' => 'logout',
        'uses' => 'Auth\LoginController@logout',
    ]);
    Route::get('/', [
        'as' => 'index',
        'uses' => 'HomeController@index_admin',
    ]);
});
