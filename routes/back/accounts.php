<?php

\Route::group([
    'middleware' => ['auth'],
    'prefix' => 'accounts'
    ], function() {

    Route::get('add', [
        'as' => 'accounts.add',
        'uses' => 'AccountsController@create',
    ]);
    Route::post('add', [
        'as' => 'accounts.add.post',
        'uses' => 'AccountsController@store',
    ]);
    Route::get('edit/{id}', [
        'as' => 'accounts.edit',
        'uses' => 'AccountsController@edit',
    ]);
    Route::get('delete/{id}', [
        'as' => 'accounts.delete',
        'uses' => 'AccountsController@delete',
    ]);
    Route::post('edit/{id}', [
        'as' => 'accounts.edit.post',
        'uses' => 'AccountsController@update',
    ]);
    Route::get('', [
        'as' => 'accounts.all',
        'uses' => 'AccountsController@all',
    ]);

    Route::get('/accounts/seller', [
        'as' => 'accounts.seller',
        'uses' => 'AccountsController@accounts_seller',
    ]);
    Route::get('/accounts/check', [
        'as' => 'accounts.check',
        'uses' => 'AccountsController@check_accounts',
    ]);
});
