<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'minime' => 50,

    'euro' => [
        'minus' => 25000,
        'plus' => 20000,
    ],

    'dollar' => [
        'minus' => 30000,
        'plus' => 27000,
    ],

];
